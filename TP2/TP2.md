# TP2 Réseaux

## 1. Setup IP

### 🌞 Mettez en place une configuration réseau fonctionnelle entre les deux machines

Grâce à Sipcalc

PC : 

```
-[ipv4 : 10.240.11.10/22] - 0

[CIDR]
Host address		- 10.240.11.10
Host address (decimal)	- 183503626
Host address (hex)	- AF00B0A
Network address		- 10.240.8.0
Network mask		- 255.255.252.0
Network mask (bits)	- 22
Network mask (hex)	- FFFFFC00
Broadcast address	- 10.240.11.255
Cisco wildcard		- 0.0.3.255
Addresses in network	- 1024
Network range		- 10.240.8.0 - 10.240.11.255
Usable range		- 10.240.8.1 - 10.240.11.254

-
```

VM :
````
-[ipv4 : 10.240.10.254/22] - 0

[CIDR]
Host address		- 10.240.10.254
Host address (decimal)	- 183503614
Host address (hex)	- AF00AFE
Network address		- 10.240.8.0
Network mask		- 255.255.252.0
Network mask (bits)	- 22
Network mask (hex)	- FFFFFC00
Broadcast address	- 10.240.11.255
Cisco wildcard		- 0.0.3.255
Addresses in network	- 1024
Network range		- 10.240.8.0 - 10.240.11.255
Usable range		- 10.240.8.1 - 10.240.11.254

-
````
Je n'ai pas utilisé la commande car j'utilise une vm mais sinon :
```
netsh interface ip set address name=“Ethernet” static 10.240.11.10 255.255.252.0
```
Pour la vm :
````
sudo nano /etc/netplan/01-network-manager-all.yaml
````
et
````
sudo netplan apply
````
### 🌞 Prouvez que la connexion est fonctionnelle entre les deux machines
````
PS C:\Users\cesar> ping 10.240.10.254

Envoi d’une requête 'Ping'  10.240.10.254 avec 32 octets de données :
Réponse de 10.240.10.254 : octets=32 temps<1ms TTL=64
Réponse de 10.240.10.254 : octets=32 temps=1 ms TTL=64
Réponse de 10.240.10.254 : octets=32 temps<1ms TTL=64
Réponse de 10.240.10.254 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 10.240.10.254:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
````
### 🌞 Wireshark it

[IMCP](./Paquet%20ICMP%20TP2.pcapng)

Type paquet ICMP ping : `0` (Echo reply) ,
Type paquet ICMP pong: `8` (Echo Request)

## II. ARP my bro
```
arp -a
```

````
Interface : 10.240.11.10 --- 0x8
  Adresse Internet      Adresse physique      Type
  10.240.10.254         08-00-27-d6-d5-63     dynamique
  10.240.11.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.5             01-00-5e-00-00-05     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

````

````

Interface : 10.33.17.8 --- 0xc
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique


````

### 🌞 Manipuler la table ARP
````
PS C:\WINDOWS\system32> netsh interface ip delete arpcache
Ok.

PS C:\WINDOWS\system32> arp -a

Interface : 192.168.198.1 --- 0x6
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.240.11.10 --- 0x8
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.33.17.8 --- 0xc
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.98.1 --- 0x17
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 172.24.160.1 --- 0x3c
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
PS C:\WINDOWS\system32> ping 10.240.10.254

Envoi d’une requête 'Ping'  10.240.10.254 avec 32 octets de données :
Réponse de 10.240.10.254 : octets=32 temps=2 ms TTL=64
Réponse de 10.240.10.254 : octets=32 temps=1 ms TTL=64
Réponse de 10.240.10.254 : octets=32 temps=1 ms TTL=64
Réponse de 10.240.10.254 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 10.240.10.254:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
PS C:\WINDOWS\system32> arp -a

Interface : 192.168.198.1 --- 0x6
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.240.11.10 --- 0x8
  Adresse Internet      Adresse physique      Type
  10.240.10.254         08-00-27-d6-d5-63     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.17.8 --- 0xc
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.98.1 --- 0x17
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 172.24.160.1 --- 0x3c
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

````
### 🌞 Wireshark it
[ARP](./Protocol%20ARP.pcapng)

## III. DHCP you too my brooo
[DORA](./DORA.pcapng)

4 trames DHCP : 

1. Discover

2. Offer :
3. Request
4. Ack

Dans les trames ``Offer`` et ``Ack`` on peut voir les adresses DNS joignable :
````
Domain Name Server: 8.8.8.8
Domain Name Server: 8.8.4.4
Domain Name Server: 1.1.1.1
````

Dans les trames ``Discover`` et ``Request`` :

l'adresse IP à utiliser : `0.0.0.0`

l'adresse IP de la passerelle par défaut : `255.255.255.255`
