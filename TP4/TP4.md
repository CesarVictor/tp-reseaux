# TP4 : TCP, UDP et services réseau

## I. First steps
### 🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP
| Nom de l'application | Protocole | IP et port du serveur distant | Port local ouvert |
|----------------------|-----------|-------------------------------|-------------------|
| Discord(Appel video) |    UDP    |    35.214.214.121:50002       |       65050       |
|       Overwatch 2    |    UDP    |     5.42.188.56:26533         |       50324       |
|       Bluestack      |    TCP    |  142.250.178.138:443          |       61846       |
|        Netflix       |    TCP    |    92.90.185.69:443           |       61373       |
|        Youtube        |    UDP   |        91.68.245.76:443       |       64821       |

````
netstat -a -b -n
````

Overwatch 2 : 
```
 [Overwatch.exe]
  TCP    10.33.17.8:55242       52.182.141.63:443      TIME_WAIT
  TCP    10.33.17.8:55281       162.247.241.14:443     ESTABLISHED
```
🦈 [captures Overwatch](./overwatch.pcapng)

Discord :
````
 [Discord.exe]
  TCP    10.33.17.8:59097       162.159.138.232:443    ESTABLISHED
````
🦈 [captures Discord](./discord.pcapng)

Pour Overwatch in-game : protocole UDP
PouR Discord en appel video : Protocole UDP

Youtube : 
````
 [chrome.exe]
  UDP    0.0.0.0:64821          91.68.245.76:443
````
🦈 [captures Youtube](./youtube.pcapng)


Netflix : 
````
[wwahost.exe]
  TCP    10.33.17.8:61373       92.90.185.69:443       ESTABLISHED
````
🦈 [captures Netflix](./netflix.pcapng)

Bluestack : 
````
 [HD-Player.exe]
  TCP    10.33.17.8:62013       142.250.178.138:443    ESTABLISHED
````
🦈 [captures Bluestack](./bluestack.pcapng)


## II.Mise en place
### 1.SSH
#### 🌞 Examinez le trafic dans Wireshark

- SSH utilise TCP

Mon pc :
````
$ netstat -a -b -n
````
````
  TCP    10.4.1.1:61444         10.4.1.11:22           ESTABLISHED
 [ssh.exe]
  TCP    10.33.17.8:139         0.0.0.0:0              LISTENING
````

la VM :
````
$ ss
````
````
tcp     ESTAB   0        0                            10.4.1.11:ssh           10.4.1.1:61444
````

🦈 [captures SSH](./capture_ssh)

### 2.Routage
-
## III.DNS
### 2.Setup

Fichier `named.conf` :
```
$ sudo cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any };
        allow-query-cache { localhost; any };

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

zone "tp4.b1" IN {
     type master;
     file "tp4.b1.db";
     allow-update { none; };
     allow-query {any; };
};

zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp4.b1.rev";
     allow-update { none; };
     allow-query { any; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```
Pour le fichier de zone : 

```
$ sudo cat /var/named/tp4.b1.db
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns-server IN A 10.4.1.201
node1      IN A 10.4.1.11
```
Fichier de zone inverse : 

```
$ sudo cat /var/named/tp4.b1.rev
$TTL 86400
@ IN SOA dns-server.tp4.b1. admin.tp4.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns-server.tp4.b1.

;Reverse lookup for Name Server
201 IN PTR dns-server.tp4.b1.
11 IN PTR node1.tp4.b1.
```
le status :
```
$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
     Active: active (running) since Sun 2022-11-27 17:02:37 CET; 12s ago
    Process: 44970 ExecStartPre=/bin/bash -c if [ ! "$DISABLE_ZONE_CHECKING" == "yes" ]; then /usr/sbin/named-checkconf -z "$NAMEDCONF"; else echo "Checkin>
    Process: 44972 ExecStart=/usr/sbin/named -u named -c ${NAMEDCONF} $OPTIONS (code=exited, status=0/SUCCESS)
   Main PID: 44973 (named)
      Tasks: 4 (limit: 5905)
     Memory: 14.5M
        CPU: 85ms
     CGroup: /system.slice/named.service
             └─44973 /usr/sbin/named -u named -c /etc/named.conf

Nov 27 17:02:37 localhost.localdomain named[44973]: network unreachable resolving './NS/IN': 2001:500:9f::42#53
Nov 27 17:02:37 localhost.localdomain named[44973]: zone localhost/IN: loaded serial 0
Nov 27 17:02:37 localhost.localdomain named[44973]: zone 1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa/IN: loaded serial 0
Nov 27 17:02:37 localhost.localdomain named[44973]: zone localhost.localdomain/IN: loaded serial 0
Nov 27 17:02:37 localhost.localdomain named[44973]: zone tp4.b1/IN: loaded serial 2019061800
Nov 27 17:02:37 localhost.localdomain named[44973]: all zones loaded
Nov 27 17:02:37 localhost.localdomain systemd[1]: Started Berkeley Internet Name Domain (DNS).
Nov 27 17:02:37 localhost.localdomain named[44973]: running
Nov 27 17:02:37 localhost.localdomain named[44973]: managed-keys-zone: Key 20326 for zone . is now trusted (acceptance timer complete)
Nov 27 17:02:37 localhost.localdomain named[44973]: resolver priming query complete
```

````
$ ss -a -n
udp    UNCONN     0       0                                         127.0.0.1:53                  0.0.0.0:*           *

````
Il ecoute sur le port 53

````
$ sudo firewall-cmd --add-port=53/udp --permanent
success
````

````
$ sudo firewall-cmd --add-port=53/tcp --permanent
success
````

### 3.Test
### Sur node1.tp4.b1

Résolution node1.tp4.b1
```
$ dig node1.tp4.b1

; <<>> DiG 9.16.23-RH <<>> node1.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 23725
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 6ac967a8af784a7b0100000063839646ead039412fdb1e2c (good)
;; QUESTION SECTION:
;node1.tp4.b1.                  IN      A

;; ANSWER SECTION:
node1.tp4.b1.           86400   IN      A       10.4.1.11

;; Query time: 1 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Sun Nov 27 17:54:15 CET 2022
;; MSG SIZE  rcvd: 85
```

```
$ dig dns-server.tp4.b1

; <<>> DiG 9.16.23-RH <<>> dns-server.tp4.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52569
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 9becae24ad2984370100000063839723356a03404bef53a7 (good)
;; QUESTION SECTION:
;dns-server.tp4.b1.             IN      A

;; ANSWER SECTION:
dns-server.tp4.b1.      86400   IN      A       10.4.1.201

;; Query time: 1 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Sun Nov 27 17:57:57 CET 2022
;; MSG SIZE  rcvd: 90
```

```
$ dig google.com

; <<>> DiG 9.16.23-RH <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 42755
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: c854f536ee4b57f001000000638396124878fa9a3b1a435b (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       142.250.179.110

;; Query time: 771 msec
;; SERVER: 10.4.1.201#53(10.4.1.201)
;; WHEN: Sun Nov 27 17:53:24 CET 2022
;; MSG SIZE  rcvd: 83
```
### Sur mon pc
```
C:\Users\cesar>nslookup node1.tp4.b1 10.4.1.201
Serveur :   dns-server.tp4.b1
Address:  10.4.1.201

Nom :    node1.tp4.b1
Address:  10.4.1.11
```
🦈 [captures DNS](./requete_dns.pcapng)