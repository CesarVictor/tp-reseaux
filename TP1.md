# TP1 - Premier pas réseau

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

**🌞 Affichez les infos des cartes réseau de votre PC**
````
ipconfig /all
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Killer(R) Wi-Fi 6 AX1650s 160MHz Wireless Network Adapter (201D2W)
   Adresse physique . . . . . . . . . . . : 08-5B-D6-C6-22-76
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::d9bf:5090:c3c3:4635%12(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.17.57(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 3 octobre 2022 09:02:22
   Bail expirant. . . . . . . . . . . . . : mardi 4 octobre 2022 08:54:58
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
   Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
   IAID DHCPv6 . . . . . . . . . . . : 117988310
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-2A-AE-8A-EF-08-5B-D6-C6-22-76
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
````
Ethernet  :

``Je n'ai pas de carte ethernet.``


**🌞 Affichez votre gateway**
````
ipconfig /all
Passerelle par défaut. . . . . . . . . : 10.33.19.254
````

**🌞 Déterminer la MAC de la passerelle**

arp -a

Interface : 10.33.17.57 --- 0xc
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique

### En graphique (GUI : Graphical User Interface)


**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

![](https://i.imgur.com/sOQzEUC.png)


## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

![](https://i.imgur.com/F2BbBC3.png)


🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

`En faisant cette opération, on peut perdre l'accès à interet car on a demandé une adresse IP qui a déjà été attribué à quelqu'un(qui lui est prioritaire).`

# II. Exploration locale en duo

## 3. Modification d'adresse IP

### Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

![](https://cdn.discordapp.com/attachments/989814795857436733/1026437371585114112/Capture_decran_2022-10-03_a_12.15.22.png)

### Vérifier à l'aide d'une commande que votre IP a bien été changée

en5: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
    options=400<CHANNEL_IO>
    ether f8:e4:3b:bd:7e:c0 
    inet6 fe80::88e:d574:ae7d:d680%en5 prefixlen 64 secured scopeid 0xa 
    inet 10.10.10.2 netmask 0xffffff00 broadcast 10.10.10.255
    nd6 options=201<PERFORMNUD,DAD>
    media: autoselect (1000baseT <full-duplex>)
    status: active


### Vérifier que les deux machines se joignent 
````
% ping 10.10.10.1
PING 10.10.10.1 (10.10.10.1): 56 data bytes
64 bytes from 10.10.10.1: icmp_seq=0 ttl=128 time=0.578 ms
64 bytes from 10.10.10.1: icmp_seq=1 ttl=128 time=0.553 ms
64 bytes from 10.10.10.1: icmp_seq=2 ttl=128 time=0.747 ms
64 bytes from 10.10.10.1: icmp_seq=3 ttl=128 time=0.680 ms
64 bytes from 10.10.10.1: icmp_seq=4 ttl=128 time=0.689 ms
64 bytes from 10.10.10.1: icmp_seq=5 ttl=128 time=0.637 ms
64 bytes from 10.10.10.1: icmp_seq=6 ttl=128 time=1.064 ms
64 bytes from 10.10.10.1: icmp_seq=7 ttl=128 time=0.661 ms
64 bytes from 10.10.10.1: icmp_seq=8 ttl=128 time=0.666 ms
64 bytes from 10.10.10.1: icmp_seq=9 ttl=128 time=0.627 ms
64 bytes from 10.10.10.1: icmp_seq=10 ttl=128 time=0.620 ms
64 bytes from 10.10.10.1: icmp_seq=11 ttl=128 time=0.699 ms
````

### Déterminer l'adresse MAC de votre correspondant

% arp -a 
? (10.10.10.1) at d8:bb:c1:1e:ae:fa on en5 ifscope [ethernet]
Image

## 4. Utilisation d'un des deux comme gateway

### Tester l'accès internet

![](https://cdn.discordapp.com/attachments/989814795857436733/1026445046255464458/Capture_decran_2022-10-03_a_12.45.56.png)

### Prouver que la connexion Internet passe bien par l'autre PC
`$ traceroute 1.1.1.1`
`traceroute to 1.1.1.1 (1.1.1.1), 64 hops max, 52 byte packets`
 `1  192.168.137.1 (192.168.137.1)  0.967 ms`


## 5. Petit chat privé

🌞 **sur le PC *serveur*** 192.168.137.1
    
J'ai entré la commande :  `nc.exe -l -p 8888`


Retour : 
vuyoht
bonjour
Bonjour.
Bonjours
Bonjourno
Bonjouras
Bonjournias
Bonjournias


Messages provenant du client : 

vuyoht
bonjour


Messages envoyés du serveur :

Bonjour.
Bonjours
Bonjourno
Bonjouras
Bonjournias
Bonjournias
 

🌞 **sur le PC *client***  IP 192.168.137.2
`% nc 192.168.137.1 8888`                        
`vuyoht 
bonjour 
Bonjour.
Bonjours
Bonjourno`
🌞 **Visualiser la connexion en cours**

`````% netstat -a -n
Active Internet connections (including servers)
Proto Recv-Q Send-Q  Local Address          Foreign Address        (state)    
tcp4       0      0  192.168.137.2.54320    192.168.137.1.8888     ESTABLISHED
tcp4       0      0  127.0.0.1.6463         *.*                    LISTEN     
tcp4       0      0  10.33.16.254.54301     162.159.136.234.443    ESTABLISHED
tcp6       0      0  *.5000                 *.*                    LISTEN     
`````
    
Coté serveur

Commande : `netstat -a -n -b`

Retour : 
````
Connexions actives

...

TCP    192.168.137.1:8888     192.168.137.2:54320    ESTABLISHED
 [nc.exe]
TCP    192.168.211.1:139      0.0.0.0:0              LISTENING

...
````
    

🌞 **Pour aller un peu plus loin**
    
On peut observer ceci :

````
TCP    192.168.211.1:139      0.0.0.0:0              LISTENING


0.0.0.0:0 veut dire que le serveur écoute toutes les adresses IP, donc toutes les interfaces. On peut donc s'y connecter en Wi-Fi.

Lancement d'un serveur netcat sur l'interface Ethernet uniquement

Commande : nc -l -p 8888 -s 192.168.137.1

Retour (message du client) : 
sdfghjk

Résultat de la commande netstat -a -n -b :

  TCP    192.168.137.1:139      0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.137.1:8888     0.0.0.0:0              LISTENING
````

## 6. Firewall

Toujours par 2.

Le but est de configurer votre firewall plutôt que de le désactiver

🌞 **Activez et configurez votre firewall**

- autoriser les `ping`
  - configurer le firewall de votre OS pour accepter le `ping`
  - aidez vous d'internet
  - on rentrera dans l'explication dans un prochain cours mais sachez que `ping` envoie un message *ICMP de type 8* (demande d'ECHO) et reçoit un message *ICMP de type 0* (réponse d'écho) en retour
- autoriser le traffic sur le port qu'utilise `nc`
  - on parle bien d'ouverture de **port** TCP et/ou UDP
  - on ne parle **PAS** d'autoriser le programme `nc`
  - choisissez arbitrairement un port entre 1024 et 20000
  - vous utiliserez ce port pour communiquer avec `netcat` par groupe de 2 toujours
  - le firewall du *PC serveur* devra avoir un firewall activé et un `netcat` qui fonctionne
  
# III. Manipulations d'autres outils/protocoles côté client


🌞**Exploration du DHCP, depuis votre PC**

`ipconfig /all`

`Bail obtenu. . . . . . . . . . . . . . : jeudi 6 octobre 2022 08:57:04`
`Bail expirant. . . . . . . . . . . . . : vendredi 7 octobre 2022 08:57:00`
`Passerelle par défaut. . . . . . . . . : 10.33.19.254`
`Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254`
    


## 2. DNS


🌞** Trouver l'adresse IP du serveur DNS que connaît votre ordinateur**
`ipconfig /all`
`Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8`

🌞 Utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main
````PS C:\Users\cesar> nslookup 8.8.8.8
Serveur :   dns.google
Address:  8.8.8.8

Nom :    dns.google
Address:  8.8.8.8
````
````
PS C:\Users\cesar> nslookup google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80e::200e
          216.58.214.174

PS C:\Users\cesar> nslookup ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::ac43:4ae2
          2606:4700:20::681a:be9
          2606:4700:20::681a:ae9
          172.67.74.226
          104.26.10.233
          104.26.11.233
````
Ynov utilise le DNS de google
L'adresse IP de ce serveur est `8.8.8.8`

  - pour l'adresse `78.73.21.21`
````
PS C:\Users\cesar> nslookup 78.73.21.21
Serveur :   dns.google
Address:  8.8.8.8

Nom :    78-73-21-21-no168.tbcn.telia.com
Address:  78.73.21.21
````
  - pour l'adresse `22.146.54.58`
````
PS C:\Users\cesar> nslookup 22.146.54.58
Serveur :   dns.google
Address:  8.8.8.8

*** dns.google ne parvient pas à trouver 22.146.54.58 : Non-existent domain
````
La premiere addresse IP utilise encore une le serveur DNS de google

# IV. Wireshark
🌞 Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

![](https://i.imgur.com/dtbJdbB.png)

un `netcat` entre vous et votre mate, branché en RJ45

![](https://media.discordapp.net/attachments/741645926447317123/1027521355773648906/unknown.png?width=1440&height=67)
une requête DNS

`nslookup google.com`
    
![](https://i.imgur.com/xsrsIaL.png)
