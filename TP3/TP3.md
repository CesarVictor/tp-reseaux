# TP3 : On va router des trucs

## I. ARP

### 1. Echange ARP

#### 🌞Générer des requêtes ARP

De John à Marcel : 

````
[user1@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.705 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.406 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.764 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=1.15 ms
64 bytes from 10.3.1.12: icmp_seq=5 ttl=64 time=0.740 ms
^C
--- 10.3.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4037ms
rtt min/avg/max/mdev = 0.406/0.752/1.149/0.236 ms
````

De Marcel à John  : 

````
[user1@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.778 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.855 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.911 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.691 ms
64 bytes from 10.3.1.11: icmp_seq=5 ttl=64 time=0.821 ms
^C
--- 10.3.1.11 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4010ms
rtt min/avg/max/mdev = 0.691/0.811/0.911/0.074 ms
````

Table ARP John :
````
[user1@localhost ~]$ ip neigh show dev enp0s8
10.3.1.12 lladdr 08:00:27:fc:62:34 STALE
10.3.1.1 lladdr 0a:00:27:00:00:47 REACHABLE
````

Table ARP Marcel :
````
[user1@localhost ~]$ ip neigh show dev enp0s8
10.3.1.1 lladdr 0a:00:27:00:00:47 REACHABLE
10.3.1.11 lladdr 08:00:27:a3:fb:b5 STALE
````
MAC de Marcel dans la table ARP de john :
```
[user1@localhost ~]$ ip neigh show 10.3.1.12
10.3.1.12 dev enp0s8 lladdr 08:00:27:fc:62:34 STALE
```
MAC de Marcel depuis marcel :
````
[user1@localhost ~]$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAUL0
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mt qlen 1000
    link/ether 08:00:27:fc:62:34 brd ff:ff:ff:ff:ff:ff

````

### 2. Analyse de trames

#### 🌞Analyse de trames

[ARP](./tp3_arp.pcapng)

## II. Routage

### 1. Mise en place du routage
#### 🌞Activer le routage sur le noeud router




#### 🌞Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping
Marcel : 
`````
sudo ip route add 10.3.1.0/24 via 10.3.2.254 dev enp0s8
`````
````
sudo systemctl restart NetworkManager   
````
````
[user1@localhost network-scripts]$ ip route show
10.3.1.0/24 via 10.3.2.254 dev enp0s8
````
John :
`````
sudo ip route add 10.3.2.0/24 via 10.3.1.254 dev enp0s8
`````
````
sudo systemctl restart NetworkManager   
````
````
[user1@localhost network-scripts]$ ip r s
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s8
````
<!-- 
````
$ sudo nano /etc/sysconfig/network-scripts/route-enp0s8
````

Marcel :
````
10.3.2.0/24 via 10.3.2.254 dev enp0s8
````

John :
````
10.3.1.0/24 via 10.3.1.254 dev enp0s8
````
 -->
Marcel ping John :
````
[user1@localhost network-scripts]$ ping  10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=63 time=1.92 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=63 time=2.14 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=63 time=1.59 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=63 time=1.75 ms
^C
--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms


````
John ping Marcel :
````
[user1@localhost network-scripts]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=2.17 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=2.36 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=2.42 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=63 time=1.90 ms
64 bytes from 10.3.2.12: icmp_seq=5 ttl=63 time=2.55 ms
^C
--- 10.3.2.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4010ms
````
### 2. Analyse de trames

#### Analyse des échanges ARP

[Routage John](./tp3_routage_john.pcapng)
| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP |`10.3.1.11`| `marcel` `08:00:27:a3:fb:b5`|`10.3.1.254`| Broadcast `FF:FF:FF:FF:FF`|
| 2     | Réponse ARP |`10.3.1.254`| `router` `08:00:27:cf:4f:df`|`10.3.1.11`| `marcel` `08:00:27:a3:fb:b5` ||
| 3     | Ping        |`10.3.1.11`|`marcel` `08:00:27:a3:fb:b5`|`10.3.2.12`|`router` `08:00:27:cf:4f:df`|
| 4     | Pong        |`10.3.2.12`|`router` `08:00:27:cf:4f:df`|`10.3.1.11`|`marcel` `08:00:27:a3:fb:b5`|
| 5     | Requête ARP |`10.3.1.254`|`router` `08:00:27:cf:4f:df`|`10.3.1.11`|`marcel` `08:00:27:a3:fb:b5`|
| 6     | Réponse ARP |`10.3.1.11`|`marcel` `08:00:27:a3:fb:b5`|`10.3.1.254`|`router` `08:00:27:cf:4f:df`|

[Routage Marcel](./tp3_routage_marcel.pcapng)

| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP |`10.3.2.12`| `john` `08:00:27:fc:62:34`|`10.3.2.254`| Broadcast `FF:FF:FF:FF:FF`|
| 2     | Réponse ARP |`10.3.2.254`| `router` `08:00:27:8d:0c:6b`|`10.3.2.12`|`john` `08:00:27:fc:62:34`||
| 3     | Ping        |`10.3.2.12`|`john` `08:00:27:fc:62:34`|`10.3.1.11`|`router` `08:00:27:8d:0c:6b`|
| 4     | Pong        |`10.3.1.11`|`router` `08:00:27:8d:0c:6b`|`10.3.2.12`|`john` `08:00:27:fc:62:34`|
| 5     | Requête ARP |`10.3.2.254`|`router` `08:00:27:8d:0c:6b`|`10.3.2.12`|`john` `08:00:27:fc:62:34`|
| 6     | Réponse ARP |`10.3.2.12`|`john` `08:00:27:fc:62:34`|`10.3.2.254`|`router` `08:00:27:8d:0c:6b`|

### 3. Accès Internet

#### 🌞Donnez un accès internet à vos machines

Marcel :
````
[user1@localhost ~]$ sudo ip route add default via 10.3.2.254 dev enp0s8
[user1@localhost ~]$ sudo systemctl restart NetworkManager
[user1@localhost ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=53 time=58.6 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=53 time=22.3 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 22.337/40.445/58.553/18.108 ms
````
````
[user1@localhost ~]$ sudo cat /etc/resolv.conf
[sudo] password for user1:
# Generated by NetworkManager
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
[user1@localhost ~]$ curl gitlab.com
[user1@localhost ~]$ dig gitlab.com

; <<>> DiG 9.16.23-RH <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 36869
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             82      IN      A       172.65.251.78

;; Query time: 28 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 12:18:58 CEST 2022
;; MSG SIZE  rcvd: 55
````
`````
[user1@localhost ~]$ ping google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=247 time=59.9 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=247 time=21.5 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=3 ttl=247 time=20.9 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 20.862/34.076/59.900/18.261 ms
`````

John :
````
[user1@localhost ~]$  sudo systemctl restart NetworkManager
[user1@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=54.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=22.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=22.0 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 21.991/32.706/54.090/15.120 ms
````
````
[user1@localhost ~]$ sudo cat /etc/resolv.conf
[sudo] password for user1:
# Generated by NetworkManager
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
[user1@localhost ~]$ curl gitlab.com
[user1@localhost ~]$ dig gitlab.com

; <<>> DiG 9.16.23-RH <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4535
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             300     IN      A       172.65.251.78

;; Query time: 35 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Fri Oct 28 12:17:24 CEST 2022
;; MSG SIZE  rcvd: 55`
````
````
[user1@localhost ~]$ ping google.com
PING google.com (142.250.201.174) 56(84) bytes of data.
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=1 ttl=112 time=56.0 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=2 ttl=112 time=23.5 ms
64 bytes from par21s23-in-f14.1e100.net (142.250.201.174): icmp_seq=3 ttl=112 time=24.3 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2008ms
rtt min/avg/max/mdev = 23.496/34.619/56.016/15.133 ms
````
| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |
|-------|------------|--------------------|-------------------------|----------------|-----------------|
| 1     | ping       |`john` `10.3.1.11`  |`marcel` `08:00:27:a3:fb:b5`| `8.8.8.8`    |`router` `08:00:27:cf:4f:df`|
| 2     | pong       | `8.8.8.8`          |`router` `08:00:27:cf:4f:df`| `john` `10.3.1.11` |`marcel` `08:00:27:a3:fb:b5`|

[Routage Internet](./tp3_routage_internet.pcapng)

## III.DHCP

### 1. Mise en place du serveur DHCP
#### 🌞Sur la machine john, vous installerez et configurerez un serveur DHCP
John :
````
[user1@localhost network-scripts]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 900;
max-lease-time 10800;
authoritative;
subnet 10.3.1.0 netmask 255.255.255.0 {
  range 10.3.1.13 10.3.1.253;
  option routers 10.3.1.254;
  option subnet-mask 255.255.255.0;
  option domain-name-server 1.1.1.1;
}
````


Bob :

````[user1@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes
[user1@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ce:ef:09 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.14/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 837sec preferred_lft 837sec
    inet6 fe80::a00:27ff:fece:ef09/64 scope link
       valid_lft forever preferred_lft forever
````

#### 🌞Améliorer la configuration du DHCP

bob :  
``ip addr del 10.3.1.14/24 dev enp0s3``

```sudo nmcli con reload
sudo nmcli con up "System enp0s8"
sudo systemctl restart NetworkManager
```

````
[user1@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ce:ef:09 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.14/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 676sec preferred_lft 676sec
    inet6 fe80::a00:27ff:fece:ef09/64 scope link
       valid_lft forever preferred_lft forever
````

````
[user1@localhost ~]$ ping 10.3.1.254
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.557 ms
64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=0.541 ms
64 bytes from 10.3.1.254: icmp_seq=3 ttl=64 time=0.723 ms
64 bytes from 10.3.1.254: icmp_seq=4 ttl=64 time=0.445 ms
^C
--- 10.3.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3064ms
rtt min/avg/max/mdev = 0.445/0.566/0.723/0.099 ms
````








